import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import us.codecraft.xsoup.Xsoup;


import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.*;

import java.util.*;
import java.lang.IllegalArgumentException;
import java.lang.NumberFormatException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ScraperParameter {
    public static String pathToConfig = "D:\\crawl_e_web\\src\\main\\java\\dien_may_cho_lon.json";
    public static String pathToFile = "result.json";
    @NotNull
    static String userInputParam(JSONObject myObj , String baseURl){
        Scanner inputParam = new Scanner(System.in);
        System.out.println("Enter your model");

        String searchParam = myObj.get("search_param").toString();
        String input = inputParam.nextLine();
        return baseURl + searchParam + input;

    }

    static List<String> RegexFinder(String regex, String text){

        List<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(regex)
                .matcher(text);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

    @NotNull
    static Integer moneyConverter(String money){
        money = money.replace(".", "").replace("đ", "").replace(",", "").replace("₫","").replace(" ","");
        return Integer.parseInt(money);
    }

    @Contract(pure = true)
    static String imageConverter(String imageUrl){
        imageUrl = imageUrl.replace("/ords/", "");
        return imageUrl;
    }


    @Nullable
    static JSONObject parse_details(String link, JSONObject myJson){
        Document document = null;
        while (1 == 1){
            try {
                document = Jsoup.connect(link).get();
                break;
            } catch (IOException e){
                System.err.println(e);
            }}
        itemExporter items = new itemExporter();
        try {
            items.itemName = Xsoup.compile(myJson.get("products_title").toString()).evaluate(document).get();
            items.imageUrl = Xsoup.compile(myJson.get("products_image").toString()).evaluate(document).get();
            if (!items.imageUrl.contains("http")){
                items.imageUrl = imageConverter(myJson.get("domain").toString() + items.imageUrl);
            }
            try {
                items.itemPrice = moneyConverter(Xsoup.compile(myJson.get("products_price").toString()).evaluate(document).get());
            }catch (NumberFormatException e ){
                items.itemPrice = 0;
                System.out.println("Exception:" + e +" in "+ document.baseUri());
            }catch (NullPointerException e){
                items.itemPrice = 0;
                System.out.println("Exception:" + e +" in "+ document.baseUri());
            }

            items.itemDescription = document.select(myJson.get("products_description").toString()).text();
            if (items.itemDescription == null){
                items.itemDescription = "";
            }
            items.itemModel = Xsoup.compile(myJson.get("products_model").toString()).evaluate(document).get();
            String[] fieldProducer = document.select(myJson.get("products_producer").toString()).text().split(" ");

            items.itemProducer = fieldProducer[fieldProducer.length -1 ];

            if (items.itemModel == null){
                List<String> matches = RegexFinder("[A-Z0-9-?-^/]{6,20}", items.itemName);
                if (matches.size() > 0) {
                    items.itemModel = items.itemProducer +" "+ matches.get(0) ;
                }
            }else {items.itemModel = items.itemProducer +" "+ items.itemModel;}

            Elements specKey = document.select((String) myJson.get("products_specifications_key"));
            Elements specValue = document.select((String) myJson.get("products_specifications_value"));
            JSONObject specs = new JSONObject();
            System.out.println("Found: " + items.itemName);
            for (int i = 0; i< specKey.size(); i++) {
                Element key = specKey.get(i);
                Element value = specValue.get(i);
                specs.put(key.text(), value.text());
            }
            items.itemId = Xsoup.compile(myJson.get("products_id").toString()).evaluate(document).get();
            JSONObject itemsJson = new JSONObject();
            itemsJson.put("Tên sản phẩm", items.itemName);
            itemsJson.put("url", document.baseUri());
            itemsJson.put("Hình ảnh", items.imageUrl);
            itemsJson.put("Giá sản phẩm", items.itemPrice);
            itemsJson.put("Mô tả", items.itemDescription);
            itemsJson.put("Mã sản phẩm", items.itemModel);
            itemsJson.put("Thông số kỹ thuật", specs);
            itemsJson.put("Hãng", items.itemProducer);

            if(items.itemPrice != 0){
                return itemsJson;
            }else return null;
        } catch (IllegalArgumentException e){
            System.out.println("Exception:" + e +" in "+ document.baseUri());
        }

    return null;

    }


    static JSONArray parse_link(@NotNull String url, String base_url, JSONObject myJson){
        Document document = null;
        JSONArray jArray = new JSONArray();
        try {
            document = Jsoup.connect(url.replace(" ", "-")).get();
        } catch (IOException e) {
            System.err.println(e);
        }
        Elements product_links = document.select(myJson.get("products_links").toString()); // for example: div[@class='my_product_class']
        for (int i = 0;i<product_links.size(); i++){
            Element link = product_links.get(i);
            String product_link = base_url + Xsoup.compile("//a/@href").evaluate(link).get();
            JSONObject jsonObj = parse_details(product_link, myJson );
            jArray.add(jsonObj);
        }
        return jArray;


    }





    public static void main(String[] args){
        JSONParser jsonParser = new JSONParser();
        try(BufferedReader reader = new BufferedReader(new FileReader(pathToConfig))) {

            Object obj = jsonParser.parse(reader);

            JSONObject json = (JSONObject) obj;

            String urlSearch = userInputParam(json, json.get("domain").toString());

            String domain = (String) json.get("domain");
            List<Integer> arrayUrls = new ArrayList<>();
            JSONArray jArray = parse_link(urlSearch, domain, json);
            List<String> list = new ArrayList<>();
            for (int u=0; u < jArray.size();u++){
                JSONObject websObj = (JSONObject) jArray.get(u);
                if (websObj != null){
                    System.out.println(websObj.toJSONString());
                    list.add(websObj.get("url").toString());
                }
            }
            arrayUrls.add(list.size());
            FileOutputStream file = new FileOutputStream(new File(pathToFile));
            file.write(jArray.toString().getBytes());

            System.out.println(arrayUrls);


        }catch (FileNotFoundException e){
            System.out.println(e);
        }catch (IOException e){
            System.out.println(e);

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


}
